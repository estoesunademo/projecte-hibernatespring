package modelsPackage;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;

public abstract class DAOGeneric <T, ID extends Serializable> implements IDAOGeneric<T, ID>	{

	Class<T> entityClass;
	
	public DAOGeneric(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	@Override
	public void persist(T entity) {
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		session.persist(entity);
		session.getTransaction().commit();
	}

	@Override
	public T searchById(ID id) {
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		T entity = session.find(entityClass, id);
		session.getTransaction().commit();
		return entity;
	}

	@Override
	public List<T> listAll() {
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		List<T> lista = session.createQuery("FROM "+entityClass.getName()).getResultList();
		session.getTransaction().commit();
		return lista;
	}

	@Override
	public void deleteById(ID id) {
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		session.delete(searchById(id));
		session.getTransaction().commit();
	}

	@Override
	public void deleteEqual(Object object) {
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		session.delete(object);
		session.getTransaction().commit();
	}
	
	public void updateEntity(Object object) {
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		session.merge(object);
		session.getTransaction().commit();
		
	}
	public void save(T entity)
	{
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		session.persist(entity);
		session.getTransaction().commit();
	}


}
