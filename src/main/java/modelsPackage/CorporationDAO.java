package modelsPackage;

public class CorporationDAO extends DAOGeneric<Corporation, Integer> {

	public CorporationDAO() {
		super(Corporation.class);
	}

	public Corporation createCorporation(String name) {
		Corporation c = new Corporation(name);
		this.save(c);
		return c;
	}
	
	public Corporation createCorporation(String name, String desc) {
		Corporation c = new Corporation(name, desc);
		this.save(c);
		return c;
	}
	
	public Corporation createCorporation(String name, String desc, int victoryp) {
		Corporation c = new Corporation(name,desc, victoryp);
		this.save(c);
		return c;
	}
	
	public void incrementVictoryPoints(Corporation c, int increment) {
		c.setVictoryPoints(c.getVictoryPoints() + increment);
		this.updateEntity(c);
	}
	
}
