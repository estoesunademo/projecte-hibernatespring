package modelsPackage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "makers")
public class Maker {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_maker")
    private int idMaker;
    @Column(name = "name_maker")
    private String name;
    @Column(name = "max_neighbours", nullable = false, columnDefinition = "int")
    private int maxNeighbours = 3;
    @Enumerated(EnumType.STRING)
    @Column(name = "typemaker")
    private Typemaker typemaker;

    //N - 1 :  lado [N] lleva el peso de la relación
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_corporation")
    private Corporation corporation;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "casillas_vecinas",
            joinColumns = @JoinColumn(name = "id_maker1"),
            inverseJoinColumns = @JoinColumn(name = "id_maker2")
    )
    private Set<Maker> vecinas = new HashSet<Maker>();

    protected Maker() {
        super();
    }

    protected Maker(String name) {
        super();
        this.name = name;
    }
    protected Maker(String name, int maxneighbours, Typemaker typemaker) {
        super();
        this.name = name;
        this.maxNeighbours = maxneighbours;
        this.typemaker = typemaker;
    }

    public int getIdMaker() {
        return idMaker;
    }

    public String getName() {
        return name;
    }

    public int getMaxNeighbours() {
        return maxNeighbours;
    }

    public Typemaker getTypemaker() {
        return typemaker;
    }

    public Corporation getCorporation() {
        return corporation;
    }

    protected void setIdMaker(int idMaker) {
        this.idMaker = idMaker;
    }

    protected void setName(String name) {
        this.name = name;
    }

    protected void setMaxNeighbours(int maxNeighbours) {
        this.maxNeighbours = maxNeighbours;
    }

    protected void setTypemaker(Typemaker typemaker) {
        this.typemaker = typemaker;
    }

    protected void setCorporation(Corporation corporation) {
        this.corporation = corporation;
    }

    public Set<Maker> getVecinas() {
        return vecinas;
    }

    protected void setVecinas(Set<Maker> vecinas) {
        this.vecinas = vecinas;
    }

    private List<Integer> ids() {
        List<Integer> i = new ArrayList<>();
        for (Maker vei : vecinas) {
            i.add(vei.idMaker);
        }
        return i;
    }
    
    private String idCorp() {
    	if (this.corporation != null) return corporation.getIdCorporation() + "";
    	return "no te corp";
    }
    
    @Override
    public String toString() {
        return "Maker{" +
                "idMaker=" + idMaker +
                ", name='" + name + '\'' +
                ", maxNeighbours=" + maxNeighbours +
                ", typemaker=" + typemaker +
                ", corporation=" + idCorp() +
                ", vecinas=" + ids() +
                '}';
    }
}
