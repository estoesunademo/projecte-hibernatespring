package modelsPackage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "corporations")
public class Corporation {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_corporation")
	private int idCorporation;
	@Column(name = "name", nullable = false, length = 50)
	private String name;
	@Column(name = "description", length = 100)
	private String description;
	@Column(name = "victoryPoints", nullable = false, length = 50)
	private int victoryPoints;

	// le tenemos que hacer la referencia a la variable que está en la entidad que
	// lleva el peso de l mapeo
	@OneToOne(mappedBy = "corporation", cascade = CascadeType.PERSIST)
	private Player player;

//	Una corporació aconsegueix moltes caselles i només són d’aquesta corporació.
	@OneToMany(mappedBy = "corporation", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
	private Set<Maker> makers = new HashSet<Maker>();

	protected Corporation() {
		super();
	}

	protected Corporation(String name) {
		super();
		this.name = name;
	}

	protected Corporation(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	protected Corporation(String name, String description, int victoryPoints) {
		super();
		this.name = name;
		this.description = description;
		this.victoryPoints = victoryPoints;
	}

	public int getIdCorporation() {
		return idCorporation;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public int getVictoryPoints() {
		return victoryPoints;
	}

	public Player getPlayer() {
		return player;
	}

	public Set<Maker> getMakers() {
		return makers;
	}

	protected void setIdCorporation(int idCorporation) {
		this.idCorporation = idCorporation;
	}

	protected void setName(String name) {
		this.name = name;
	}

	protected void setDescription(String description) {
		this.description = description;
	}

	protected void setVictoryPoints(int victoryPoints) {
		this.victoryPoints = victoryPoints;
	}

	protected void setPlayer(Player player) {
		this.player = player;
	}

	protected void setMakers(Set<Maker> makers) {
		this.makers = makers;
	}

	protected String playerName() {
		return player != null ? player.getName() : "aquesta corporation no te player";
	}

	private List<Integer> ids() {
		List<Integer> i = new ArrayList<>();
		for (Maker m : makers) {
			i.add(m.getIdMaker());
		}
		return i;
	}

	@Override
	public String toString() {
		return "Corporation [idCorporation=" + idCorporation + ", name=" + name + ", description=" + description
				+ ", victoryPoints=" + victoryPoints + ", player=" + playerName() + ", makers=" + ids() + "]";
	}

}
