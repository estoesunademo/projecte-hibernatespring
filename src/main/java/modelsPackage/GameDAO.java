package modelsPackage;

import java.util.LinkedList;
import java.util.Queue;

public class GameDAO extends DAOGeneric<Game, Integer> {

	public GameDAO() {
		super(Game.class);
	}

	public Game createGame() {
		Game g = new Game();
		this.save(g);
		return g;
	}

	public boolean isEndedGame(Game g) {
		Game game = searchById(g.getIdGame());
		int assolits = 0;

		if (game.getTemperature() == 0) {
			assolits++;
		}
		if (game.getOxygen() == 14) {
			assolits++;
		}
		MakerDAO md = new MakerDAO();
		boolean flag = false;
		for (Maker m : md.getMakersByType(Typemaker.OCEA)) {
			if (m.getCorporation() != null)
				flag = true;
		}
		if (flag)
			assolits++;
		return assolits >= 2;
	}

	public void setWinner(Player p, Game g) {
		g.setWinnerPlayer(p);
		updateEntity(g);
	}

	public void incrementTemperature(int i, Game g) {
		Game game = searchById(g.getIdGame());
		game.setTemperature(game.getTemperature() + i);
		updateEntity(game);
	}

	public void incrementOxygen(int i, Game g) {
		Game game = searchById(g.getIdGame());
		game.setOxygen(game.getOxygen() + i);
		updateEntity(game);
	}

	public Queue<Player> getPlayersQueue(Game g) {
		Game game = searchById(g.getIdGame());
		Queue players = new LinkedList<Player>();
		for (Player p : game.getPlayersInGame()) {
			players.offer(p);
		}
		return players;
	}

}